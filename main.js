console.log("Welcome welcome.");


$(document).ready(function() {
  esconderPajaro3();
  esconderMarcador();

  //Esconder el contenido de body
  function start() {
    $("body").children().hide();
  }

//////////////////////////////////////////////////////////

  //MARCADOR
  //Mostrar marcador
  function mostrarMarcador() {
    $("#marcador").show();
    $("#marcador-box").show();
  }
  //Esconder marcador
  function esconderMarcador() {
    $("#marcador").hide();
  }


  //PAJARO1
  //Mostrar pajaro1
  function mostrarPajaro1() {
    $(".pajaro1").show();
  }
  //Esconder pajaro1
  function esconderPajaro1() {
    $(".pajaro1").hide();
  }

  //PAJARO2
  //Mostrar pajaro2
  function  mostrarPajaro2() {
    $(".pajaro2").show();
  }
  //Esconder pajaro2
  function esconderPajaro2() {
    $(".pajaro2").hide();
  }

  //PAJARO3
  //Mostrar pajaro3
  function  mostrarPajaro3() {
    $(".pajaro3").show();
  }
  //Esconder pajaro3
  function esconderPajaro3() {
    $(".pajaro3").hide();
  }


  //PAJARO4
  //Mostrar pajaro4
  function  mostrarPajaro4() {
    $(".pajaro4").show();
  }
  //Esconder pajaro4
  function esconderPajaro4() {
    $(".pajaro4").hide();
  }

  function incrementarMarcador() {
    var el = parseInt($('#marcador').text());
    $('#marcador').text(el+1);

    $("#marcador").blur();
  }

  $('btn').click(function() {
});

////////////////////////////////////////////////////////



  //Cuando click en #start llama la function start
  $("#start").click(function() {
    start();
    mostrarMarcador();

    // Make marcador unfocusable
    //document.getElementById("marcador").setAttribute("tabIndex", "-1");
    //document.getElementById("marcador-box").setAttribute("tabIndex", "-1");


    //Hacer aparecer #pajaro1 de repente en el body
    function generator1() {
      $("body").append('<div class="pajaro1"></div>');

      //Matar pajaro1
      $(".pajaro1").click(function() {
        esconderPajaro1();
        incrementarMarcador();
      });
    }
     setInterval(function(){
      generator1();
    }, 3500);

    //Hacer aparecer #pajaro2 de repente en el body
    function generator2() {
      $("body").append('<div class="pajaro2"></div>');

      //Matar pajaro2
      $(".pajaro2").click(function() {
        esconderPajaro2();
        incrementarMarcador();
      });
    }

     setInterval(function(){
      generator2();
    }, 4000);

    //Hacer aparecer #pajaro3 de repente en el body
    function generator3() {
      $("body").append('<div class="pajaro3"></div>');

      //Matar pajaro3
      $(".pajaro3").click(function() {
        esconderPajaro3();
        incrementarMarcador();
      });
    }

     setInterval(function(){
    generator3();
  }, 3000);

  //Hacer aparecer #pajaro4 de repente en el body
  function generator4() {
    $("body").append('<div class="pajaro4"></div>');

    //Matar pajaro4
    $(".pajaro4").click(function() {
      esconderPajaro4();
      incrementarMarcador();
    });
  }

   setInterval(function(){
  generator4();
  }, 5000);

  });

});
